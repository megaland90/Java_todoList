package e.mottet_t.java_todolist.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mottet_t on 18/02/18.
 */

public class ProjectsList
{

    private static  ProjectsList projectsList;
    public List<ProjectModels> projects;
    private ProjectModels selected;


    public static ProjectsList getInstance()
    {
        if (projectsList == null)
        {
            projectsList = new ProjectsList();
        }
        return  projectsList;

    }

    private ProjectsList()
    {
        this.projects = new ArrayList<>();
    }

    public void select(ProjectModels project)
    {
        this.selected = project;
    }

    public  ProjectModels getSelected()
    {
        return  this.selected;
    }
}
