package e.mottet_t.java_todolist;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import e.mottet_t.java_todolist.Adapter.ProjectAdapter;
import e.mottet_t.java_todolist.Adapter.TodoAdapter;
import e.mottet_t.java_todolist.models.ProjectModels;
import e.mottet_t.java_todolist.models.ProjectsList;
import e.mottet_t.java_todolist.models.TodoModels;

public class Todo extends AppCompatActivity {


    private ListView listView;
    private TodoAdapter toDoAdapter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    return true;
                    break;
                case R.id.navigation_notifications:
                    return true;
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        this.listView = (ListView) findViewById(R.id.todoList);

        this.toDoAdapter = new TodoAdapter(Todo.this, ProjectsList.getInstance().getSelected().todo);
        this.listView.setAdapter(this.toDoAdapter);
    }


    public void onClickAddProject(View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Todo");


        final Todo todo = this;
        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                ProjectsList.getInstance().getSelected().todo.add(new TodoModels(input.getText().toString(), false));
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

        this.toDoAdapter.notifyDataSetChanged();
    }
}
