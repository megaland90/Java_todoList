package e.mottet_t.java_todolist.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import java.util.List;

import e.mottet_t.java_todolist.R;
import e.mottet_t.java_todolist.Todo;
import e.mottet_t.java_todolist.models.ProjectModels;
import e.mottet_t.java_todolist.models.ProjectsList;

/**
 * Created by mottet_t on 02/02/18.
 */

class ProjectViewHolder
{
    public TextView name;
    public Button delete;
}


public class ProjectAdapter extends ArrayAdapter<ProjectModels>
{
    public ProjectAdapter(@NonNull Context context, @NonNull List<ProjectModels> objects) {
        super(context, 0, objects);
    }



    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.project_list_item,parent, false);
        }

        ProjectViewHolder viewHolder = (ProjectViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ProjectViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.projectName);
            viewHolder.delete = (Button) convertView.findViewById(R.id.btnDeleteTodo);
            convertView.setTag(viewHolder);
        }

        final ProjectModels project = getItem(position);
        viewHolder.name.setText(project.getName());
        final ProjectAdapter adp = this;
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectsList.getInstance().projects.remove(project);
                adp.notifyDataSetChanged();
            }
        });
        final Context context = this.getContext();
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectsList.getInstance().select(project);
                Intent intent = new Intent(context, Todo.class);
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
