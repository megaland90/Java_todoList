package e.mottet_t.java_todolist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
    }

    public void onClickLogin(View view)
    {
        Intent intent = new Intent(this, Home.class);

        startActivity(intent);

        //setContentView(R.layout.activity_home);
    }
}
