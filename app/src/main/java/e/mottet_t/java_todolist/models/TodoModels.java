package e.mottet_t.java_todolist.models;

/**
 * Created by mottet_t on 02/02/18.
 */

public class TodoModels
{
    private String name;
    private boolean isCheck;

    public TodoModels(String name, boolean isCheck) {

        this.name = name;
        this.isCheck = isCheck;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}