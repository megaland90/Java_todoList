package e.mottet_t.java_todolist.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mottet_t on 02/02/18.
 */

public class ProjectModels {
    private String name;
    public List<TodoModels> todo;

    public ProjectModels(String name)
    {
        this.name = name;
        this.todo = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
