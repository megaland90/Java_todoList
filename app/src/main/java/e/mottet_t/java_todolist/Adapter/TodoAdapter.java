package e.mottet_t.java_todolist.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import e.mottet_t.java_todolist.R;
import e.mottet_t.java_todolist.Todo;
import e.mottet_t.java_todolist.models.ProjectsList;
import e.mottet_t.java_todolist.models.TodoModels;

/**
 * Created by mottet_t on 02/02/18.
 */

class TodoViewHolder
{
    public CheckBox checkBox;
    public Button delete;
}


public class TodoAdapter extends ArrayAdapter<TodoModels>
{
    public TodoAdapter(@NonNull Context context, @NonNull List<TodoModels> objects) {
        super(context, 0, objects);
    }



    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.todo_list_item,parent, false);
        }

        TodoViewHolder viewHolder = (TodoViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new TodoViewHolder();
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            viewHolder.delete = (Button) convertView.findViewById(R.id.btnDeleteTodo);
            convertView.setTag(viewHolder);
        }

        final TodoModels todo = getItem(position);
        viewHolder.checkBox.setText(todo.getName());
        viewHolder.checkBox.setChecked(todo.isCheck());
        final TodoAdapter adp = this;
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectsList.getInstance().getSelected().todo.remove(todo);
                adp.notifyDataSetChanged();
            }
        });

        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todo.setCheck(!todo.isCheck());
                adp.notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
